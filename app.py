# Import required Libraries
from flask import Flask, request, jsonify
import json

from get_top_skills import GetTopSkills
from question_similarity import QuestionSimilarity

ts_obj = GetTopSkills()
q_obj = QuestionSimilarity()

app = Flask(__name__)


@app.route('/', methods=['GET'])
def check():
    return "API Working"


@app.route('/question_similarity', methods=['POST'])
def get_questions():
    """

    :return: json of 50 questions along with their score
    """
    data = json.loads(request.get_data().decode('utf-8'), strict=False)
    text = data['text']

    # Get top n skills
    top_skills = ts_obj.get_top_skills(text)

    print("Getting Questions Now")
    # Fetch questions using wmd similarity
    # questions = q_obj.wmd_similarity(top_skills)

    # Fetch questions using cosine similarity scores
    results = q_obj.cosine_similarity(top_skills)

    # Return questions in json format
    return jsonify({"Results": results})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
