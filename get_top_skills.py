# from datetime import datetime

import os
import requests

base_path = os.getcwd()


class GetTopSkills:

    def __init__(self):
        self.api_url_get_hashing = "http://ai-model.lexinsight.com/get_hashing"
        self.api_url_industry_classify = "http://ai-model.lexinsight.com/industry_classify"
        self.api_url_function_classify = "http://ai-model.lexinsight.com/function_classify"
        self.api_url_top_skills = "http://ai-model.lexinsight.com/top_skills"
        # self.api_url_similarity = "http://ai-model.lexinsight.com/similarity"
        self.api_url_similarity = "http://192.168.1.24:5000/similarity"

    @staticmethod
    def api_calls(api_url, input_val):
        """
        Generic API call for getting hashed text

        :param api_url: url of API being called
        :param input_val: input text(job description)
        :return: hashed text

        """
        response = requests.request("POST", api_url, json=input_val)

        return response.json()

    @staticmethod
    def api_call_top_skills(api_url, jd_hash, industry_hash, function_hash):
        """

        :param api_url: url for API to be called
        :param jd_hash: hashed job description
        :param industry_hash: hashed industry
        :param function_hash: hashed function
        :return: list of skills
        """
        l1 = []
        l2 = []

        d = {"Industry": industry_hash, "Function": function_hash, "hashed_description": jd_hash}

        l2.append(d)

        l1.append(l2)

        d1 = {'data': l1}

        response = requests.request("POST", api_url, json=d1)

        output = response.json()

        lst = output[0][0]
        skills_list = [d['skill'] for d in lst]

        skills_list = skills_list[:20]
        # skills_list1 = [skills_list]

        return skills_list

    def get_top_skills(self, jd_text):
        """

        :param jd_text: Job Description dictionary
        :return: top skills
        """
        l1 = []
        l2 = [jd_text]

        l1.append(l2)

        d1 = {"text": jd_text}
        d2 = {"rjd_list": l1}

        # Get JD hash
        jd_hash_output = self.api_calls(self.api_url_get_hashing, d1)
        jd_hash = jd_hash_output['Hashed text']  # hashing function

        # Get Industry
        industry_output = self.api_calls(self.api_url_industry_classify, d2)
        industry = industry_output[0][0][0]['clazz']

        # Get Function
        function_output = self.api_calls(self.api_url_function_classify, d2)
        function = function_output[0][0][0]['clazz']

        d3 = {"text": industry}
        d4 = {"text": function}

        # Convert industry to industry hash
        industry_hash_output = self.api_calls(self.api_url_get_hashing, d3)
        industry_hash = industry_hash_output['Hashed text']

        # Convert function to function hash
        function_hash_output = self.api_calls(self.api_url_get_hashing, d4)
        function_hash = function_hash_output['Hashed text']

        # Get top skills using industry hash, function hash and jd hash
        top_skills = self.api_call_top_skills(self.api_url_top_skills, jd_hash, industry_hash, function_hash)

        # Return Top n Skills
        top_n_skills = top_skills[:10]

        return top_n_skills

    # Function to get similarity score between JD and Questions
    def get_similarity_scores(self, list_hash, jd_hash):
        list_hash = [list_hash]
        data = {"jd": jd_hash,
                "rjd_list": list_hash}

        response = requests.request("POST", self.api_url_similarity, json=data)

        output = response.json()

        return output
