# Import required libraries
from nltk.corpus import stopwords
from operator import itemgetter

# Initialize WmdSimilarity.
from gensim.similarities import WmdSimilarity

import os
import pandas as pd
from get_top_skills import GetTopSkills

ts_obj = GetTopSkills()

stop_words = stopwords.words('english')

# Base project directory
path = os.path.dirname(os.path.abspath(__file__))


class QuestionSimilarity:
    """ Class to Find Similar Questions """

    def __init__(self):
        # Path to questions file
        self.q_corpus = pd.read_csv(path + '/data/corpus_q.csv')

        # Path to Questions hash file
        self.q_hash_file = pd.read_csv(path + '/data/q_list.csv')

    def cosine_similarity(self, skills):
        """

        :param skills: list of skills
        :return: list of similar questions using cosine similarity

        """

        df_hash = self.q_hash_file.where((pd.notnull(self.q_hash_file)), None)
        list_hash = df_hash.question_hash.to_list()

        # Create a list of Questions from DF
        ques_list = self.q_corpus.Question.to_list()

        # Separate skills with space
        skills_list = [" ".join(word for word in skills)]

        # use skills and get similarity to get scores
        scores = ts_obj.get_similarity_scores(list_hash, skills_list[0])

        scores1 = scores[0]

        # Replace empty list from scores with zero (for sorting)
        for n, i in enumerate(scores1):
            if i == []:
                scores1[n] = 0

        # get questions from similarity scores
        result = dict(zip(ques_list, scores1))

        # Sort scores in descending order: output- list of tuples (question, score)
        sorted_result = sorted(result.items(), key=itemgetter(1), reverse=True)

        # Select top n results
        sorted_result = sorted_result[:50]

        final_list = []
        for question, score in sorted_result:
            data = {
                "Question": question,
                "Score": score
            }
            final_list.append(data)

        return final_list

    def wmd_similarity(self, skills, num_best=100):

        wmd_corpus = []

        for text in self.corpus:
            # print(text)

            text = self.preprocess(text)
            wmd_corpus.append(text)

        instance = WmdSimilarity(wmd_corpus, self.model, num_best=num_best)
        sims = instance[skills]

        question_list = []
        for i in range(50):
            print()
            score = sims[i][1]
            question = self.corpus[sims[i][0]]

            data = {
                "Question": question,
                "score": score
            }
            question_list.append(data)

        return question_list
