from abc import ABC, abstractmethod
import pickle

class Model(ABC):
#    minSimilarity = 0.1
#     num_responses = 5
#     num_iter = 2000
    # default_responses = ["Sorry I dont understand, could you explain that differently?",
    #                      "Sorry I didn't get that, could you explain that in a different way?",
    #                      "I think I will need to pass you on to a specialist for that one."]
    vectorizer = None
    vocabulary = None
    responses = None
    bot_entities = None
    # text_responses = []

    @staticmethod
    def load(location):
        pkl_file = open(location, 'rb')

        model = pickle.load(pkl_file)
        pkl_file.close()
        return model

    @abstractmethod
    def predict(self, text,reponse_candidates,numResponse):
        pass

    @abstractmethod
    def fit(self, data):
        pass

    @staticmethod
    def save(model,location):
        output = open(location, 'wb')
        pickle.dump(model, output, -1)
        output.close()

    def set_vectorizer(self, vectorizer):
        self.vectorizer = vectorizer

    def info(self):
        return self.__name__

class ModelFactory(object):
    # Create based on class name:
    @staticmethod
    def factory(vectorizer, model_parameters):
        if model_parameters["model_type"] == "BaseModel": return BaseModel(vectorizer,model_parameters)
        if model_parameters["model_type"] == "DualEncoder": return DualEncoderModel(vectorizer,model_parameters)
        assert 0, "Bad Model creation: " + model_parameters["model_type"]


class BaseModel(Model):
    model_contexts = None
    dimension_type_name = None