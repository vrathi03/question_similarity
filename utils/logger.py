# Import Required Libraries
import logging
from datetime import datetime

from utils.config import config


LOG_DIR_PATH = config['LOGGING_SECTION']['LOG_DIR_PATH']
logging_level = config.get('LOGGING_SECTION', 'LOG_LEVEL')


logging.basicConfig(filename=LOG_DIR_PATH + 'question-similarity-{:%Y-%m-%d}.log'.format(datetime.now()),
                    level=logging_level, format='%(asctime)s %(filename)s %(levelname)-4s  %(lineno)-4s  %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%SZ')

logger = logging.getLogger(__name__)
